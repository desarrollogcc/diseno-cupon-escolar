﻿Imports System.Drawing.Printing
Imports System.Drawing
Imports Zen.Barcode
Imports System.Configuration
Imports System.IO.Ports
Imports System.Drawing.Imaging
Imports ThoughtWorks.QRCode
Imports ThoughtWorks.QRCode.Codec
Imports ThoughtWorks.QRCode.Codec.Data


Module Module1
    Private MontoRegalo As String
    Private MontoMinimo As String
    Private Codigo As String
    Private Desde As String
    Private Hasta As String
    Private mes As String
    Private anio As String
    Private vigencia As String
    ' cambios para cupon doble
    Private CodigoCuponDoble As String
    Private CuponVisible As String

    Sub Main(args() As String)


        '' Prueba()
        Dim log As New Bitacora


        If args.Length <> 0 Then


            Dim pkInstalledPrinters As New List(Of String)
            log.Log("Impresoras instaladas disponibles**********")
            For i = 0 To PrinterSettings.InstalledPrinters.Count - 1
                log.Log("Impresora: " & PrinterSettings.InstalledPrinters.Item(i))
            Next

            Try
                MontoRegalo = 10
                Codigo = args(0).Split("|")(0).ToString
                vigencia = args(0).Split("|")(1).ToString

                log.Log(String.Format("Parametros recibidos: {0} {1} ", Codigo, vigencia))
                Dim Efeticket As New PrintDocument
                Efeticket.PrinterSettings.PrinterName = ConfigurationManager.AppSettings.Get("NombreImpresora")
                log.Log("Nombre de la impresora Configurada " & ConfigurationManager.AppSettings.Get("NombreImpresora"))
                Efeticket.DefaultPageSettings.Landscape = False
                AddHandler Efeticket.PrintPage, AddressOf Efeticket_PrintPage
                Efeticket.Print()

                log.Log("Imprimiendo ticket")

            Catch ex As Exception
                log.Log("Error: " + ex.Message.ToString)
            Finally
                log.Log("Termina Proceso")
            End Try
        End If
    End Sub


    Private Sub Efeticket_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)




        'CODIGO QR 13-02-19
        Dim textlocation As Point
        Dim imgeLocation As Point
        Dim newImage As Image
        Dim ImgQR As Image
        imgeLocation = New Point(0, 135)
        Dim QR As QRCodeEncoder = New QRCodeEncoder
        QR.QRCodeEncodeMode = Codec.QRCodeEncoder.ENCODE_MODE.BYTE
        QR.QRCodeVersion = 0
        QR.QRCodeScale = 5
        ImgQR = QR.Encode(ConfigurationManager.AppSettings.Get("protocolo").ToString & ConfigurationManager.AppSettings.Get("Pagina").ToString & ConfigurationManager.AppSettings.Get("variable").ToString & Codigo)


        Dim log As New Bitacora
        log.Log("Inicia diseño del ticket")

        'Dim drawcode As Code128BarcodeDraw = BarcodeDrawFactory.Code128WithChecksum

        'header
        imgeLocation = New Point(0, 0)
        newImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory & "Cabecero272.png")
        e.Graphics.DrawImage(newImage, imgeLocation)

        'info
        'dskool.com y obtén
        textlocation = New Point(15, 180)
        e.Graphics.DrawString("Regístrate en " & ConfigurationManager.AppSettings.Get("Paginaheader").ToString, New Font("Arial Black", 10), Brushes.Black, textlocation)
        textlocation = New Point(15, 195)
        e.Graphics.DrawString("y obtén: ", New Font("Arial Black", 10), Brushes.Black, textlocation)

        Select Case MontoRegalo.Length
            Case 2
                textlocation = New Point(0, 175)
                e.Graphics.DrawString(MontoRegalo, New Font("Arial Black", 80, FontStyle.Bold), Brushes.Black, textlocation)
                textlocation = New Point(150, 175)
                e.Graphics.DrawString("%", New Font("Arial Black", 80, FontStyle.Bold), Brushes.Black, textlocation)
                textlocation = New Point(170, 300)
                e.Graphics.DrawString("de descuento", New Font("Arial Black", 10, FontStyle.Bold), Brushes.Black, textlocation)
            Case 3
                textlocation = New Point(30, 30)
                e.Graphics.DrawString(MontoRegalo, New Font("Calibri", 85, FontStyle.Bold), Brushes.Black, textlocation)
                textlocation = New Point(230, 60)
                e.Graphics.DrawString("00", New Font("Calibri", 25, FontStyle.Bold), Brushes.Black, textlocation)
            Case 5
                textlocation = New Point(50, 80)
                e.Graphics.DrawString(MontoRegalo, New Font("Calibri", 50, FontStyle.Bold), Brushes.Black, textlocation)
                textlocation = New Point(220, 80)
                e.Graphics.DrawString("00", New Font("Calibri", 25, FontStyle.Bold), Brushes.Black, textlocation)
            Case 6
                textlocation = New Point(35, 65)
                e.Graphics.DrawString(MontoRegalo, New Font("Calibri", 50, FontStyle.Bold), Brushes.Black, textlocation)
                textlocation = New Point(240, 70)
                e.Graphics.DrawString("00", New Font("Calibri", 25, FontStyle.Bold), Brushes.Black, textlocation)
        End Select

        textlocation = New Point(0, 325)
        e.Graphics.DrawString("En uniformes y artículos escolares", New Font("Arial Black", 10, FontStyle.Bold), Brushes.Black, textlocation)

        If ConfigurationManager.AppSettings.Get("TipoTienda").ToString().ToUpper = "DS" Then
            newImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory & "ds.png")
            imgeLocation = New Point(100, 360)
            e.Graphics.DrawImage(newImage, imgeLocation)
        ElseIf ConfigurationManager.AppSettings.Get("TipoTienda").ToUpper().ToUpper = "WW" Then
            newImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory & "WW.png")
            imgeLocation = New Point(1, 280)
            e.Graphics.DrawImage(newImage, imgeLocation)

        End If

        imgeLocation = New Point(0, 470)
        newImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory & "divider2.png")
        e.Graphics.DrawImage(newImage, imgeLocation)

        textlocation = New Point(90, 490)
        e.Graphics.DrawString("ES MUY ", New Font("Arial", 12), Brushes.Black, textlocation)
        textlocation = New Point(155, 487)
        e.Graphics.DrawString("FÁCIL", New Font("Arial Black", 12), Brushes.Black, textlocation)

        textlocation = New Point(10, 510)
        e.Graphics.DrawString("1.", New Font("Arial", 9), Brushes.Black, textlocation)
        textlocation = New Point(20, 510)
        e.Graphics.DrawString("Escanea", New Font("Arial", 9, FontStyle.Bold), Brushes.Black, textlocation)
        textlocation = New Point(75, 510)
        e.Graphics.DrawString("este código con tu móvil", New Font("Arial", 9), Brushes.Black, textlocation)

        'newImage = drawcode.Draw(Codigo, 50)
        imgeLocation = New Point(55, 530)
        e.Graphics.DrawImage(ImgQR, imgeLocation)

        textlocation = New Point(10, 710)
        e.Graphics.DrawString("O ", New Font("Arial", 9), Brushes.Black, textlocation)
        textlocation = New Point(25, 710)
        e.Graphics.DrawString("visita " & ConfigurationManager.AppSettings.Get("Pagina").ToString, New Font("Arial", 9, FontStyle.Bold), Brushes.Black, textlocation)
        textlocation = New Point(161, 710)
        e.Graphics.DrawString("e ingresa este folio", New Font("Arial", 9), Brushes.Black, textlocation)

        textlocation = New Point(15, 730)
        e.Graphics.DrawString(Codigo, New Font("Arial", 12), Brushes.Black, textlocation)

        textlocation = New Point(10, 750)
        e.Graphics.DrawString("2. ", New Font("Arial", 9), Brushes.Black, textlocation)
        textlocation = New Point(20, 750)
        e.Graphics.DrawString("Registra tus datos", New Font("Arial", 9, FontStyle.Bold), Brushes.Black, textlocation)
        textlocation = New Point(135, 750)
        e.Graphics.DrawString("antes del ", New Font("Arial", 9), Brushes.Black, textlocation)
        textlocation = New Point(20, 765)
        e.Graphics.DrawString(ConfigurationManager.AppSettings.Get("Registro").ToString, New Font("Arial", 9), Brushes.Black, textlocation)

        textlocation = New Point(10, 780)
        e.Graphics.DrawString("3. ", New Font("Arial", 9), Brushes.Black, textlocation)
        textlocation = New Point(20, 780)
        e.Graphics.DrawString("Recibe tu cupón", New Font("Arial", 9, FontStyle.Bold), Brushes.Black, textlocation)
        textlocation = New Point(125, 780)
        e.Graphics.DrawString("vía email o SMS", New Font("Arial", 9), Brushes.Black, textlocation)

        textlocation = New Point(10, 795)
        e.Graphics.DrawString("4. ", New Font("Arial", 9), Brushes.Black, textlocation)
        textlocation = New Point(20, 795)
        e.Graphics.DrawString("Presenta el cupón", New Font("Arial", 9, FontStyle.Bold), Brushes.Black, textlocation)
        textlocation = New Point(135, 795)
        e.Graphics.DrawString("para obtener tu beneficio", New Font("Arial", 9), Brushes.Black, textlocation)
        textlocation = New Point(20, 810)
        e.Graphics.DrawString("antes del", New Font("Arial", 9), Brushes.Black, textlocation)
        textlocation = New Point(76, 810)
        e.Graphics.DrawString(vigencia, New Font("Arial", 9), Brushes.Black, textlocation)

        textlocation = New Point(35, 830)
        e.Graphics.DrawString("Tu cupón es personal e intransferible.", New Font("Arial", 9), Brushes.Black, textlocation)
        textlocation = New Point(38, 843)
        e.Graphics.DrawString("Cupón válido para una sola compra ", New Font("Arial", 9), Brushes.Black, textlocation)
        textlocation = New Point(40, 855)
        e.Graphics.DrawString("de artículos escolares y uniformes.", New Font("Arial", 9), Brushes.Black, textlocation)
        textlocation = New Point(25, 868)
        e.Graphics.DrawString("del " & ConfigurationManager.AppSettings.Get("Uso").ToString, New Font("Arial", 9), Brushes.Black, textlocation)
        textlocation = New Point(33, 885)
        e.Graphics.DrawString("Descuento no acumulable con otros cupones", New Font("Arial", 8), Brushes.Black, textlocation)

        'footer
        imgeLocation = New Point(0, 898)
        newImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory & "divider2.png")
        e.Graphics.DrawImage(newImage, imgeLocation)

    End Sub


End Module

